
jQuery(document).ready(function(e) {
    jQuery(".scroll a").on("click", function(e) {
        e.preventDefault();
        var n = jQuery(this).attr("href");
        jQuery("html, body").animate({
            scrollTop: jQuery(n).offset().top 
        }, "slow")
    })
})

var testmonialThumbs = new Swiper(".testmonial-thumbs", {
    centeredSlides: false,
    centeredSlidesBounds: true,
    slidesPerView: 4,
    watchOverflow: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    direction: 'vertical'
  });
  
  var testmonialMain = new Swiper(".testmonial-main", {
    watchOverflow: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    preventInteractionOnTransition: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    effect: 'fade',
      fadeEffect: {
      crossFade: true
    },
    thumbs: {
      swiper: testmonialThumbs
    }
  });
  
  testmonialMain.on('slideChangeTransitionStart', function() {
    testmonialThumbs.slideTo(testmonialMain.activeIndex);
  });
  
  testmonialThumbs.on('transitionStart', function(){
    testmonialMain.slideTo(testmonialThumbs.activeIndex);
  });


  $(".navbar-toggler").click(function(){
    $("html").toggleClass("fixpage");
    $(".overlay-div").toggleClass("show1");
  });
  // $(".navbar-top, .af-navbar, .overlay-div").click(function(){
  //   $("html").removeClass("fixpage");
  //   $(".overlay-div").removeClass("show1");
  //   $(".navbar-collapse").removeClass("show");
  //   $(".navbar-toggler").removeClass("collapsed");
  // });

//   $(document).click(function(e) 
// {
//     var container = $(".navbar-collapse");

//     // if the target of the click isn't the container nor a descendant of the container
//     if (!container.is(e.target) && container.has(e.target).length === 0) 
//     {
//         container.removeClass("show");
//         $(".navbar-toggler").removeClass("collapsed");
//         $("html").removeClass("fixpage");
//     }
// });