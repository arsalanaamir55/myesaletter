
<section class="navbar-top">
	<div class="container navtop-inner">
		<div class="brand-name">
			<a href="javascript:void(0)"><img src="images/MyESA-letter-color-logo.svg" alt=""></a>
		</div>
		<div class="top-nav-item">
			<ul>
				<li><p class="mr-4 mr-md-0"><span><i class="fa fa-phone d-inline d-md-none" aria-hidden="true"></i></span><span class="d-none d-md-inline">Call/text:</span> (800) 372-0148</p></li>
				<li><p ><span><i class="fa fa-envelope d-inline d-md-none" aria-hidden="true"></i></span><span class="d-none d-md-inline">Email:</span> info@MyESAletter.com</p></li>
				<li><a href="">Login | Signup</a></li>
			</ul>
		</div>
	</div>
</section>
<section class="af-navbar">
	<div class="mynavbar">
		<div class="container ">
		<nav class="navbar navbar-expand-lg navbar-light">
			<div class="brand-name">
				<a href="javascript:void(0)"><img src="images/MyESA-letter-color-logo.svg" alt=""></a>
			</div>
			<button type="button" class="navbar-toggler x collapsed" data-toggle="collapse" data-target="#navbarSupportedContent">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button> -->

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav_items"><a href="javascript:void(0)">Home</a></li>
					<li class="nav_items"><a href="javascript:void(0)">Emotional Support Animals</a></li>
					<li class="nav_items"><a href="javascript:void(0)">Who Qualifies?</a></li>
					<li class="nav_items"><a href="javascript:void(0)">Pricing</a></li>
					<li class="nav_items"><a href="javascript:void(0)">Benefits of ESA</a></li>
					<li class="nav_items"><a href="javascript:void(0)">Sample ESA Letter</a></li>
					<li class="nav_items"><a href="javascript:void(0)">Why Us?</a></li>
					<li class="nav_items"><a href="javascript:void(0)">FAQ's</a></li>
					<li class="nav_items"><a href="javascript:void(0)">Testimonials</a></li>
					<li class="nav_items"><a href="javascript:void(0)">Blogs</a></li>
					<li class="nav_items login-signup"><a href="javascript:void(0)">Login / Signup</a></li>
				</ul>
			</div>
		</nav>
		</div>
	</div>
</section>