<?php include("header.php"); ?>
<?php include("navbar.php"); ?>

<section class="af-banner-web">
  <div class="container">
      <div class="banner-inner">
        <h1>We Help People Certify <br> Their Pet As An Emotional Support Animal.</h1>
        <h2>FAST, AUTHENTIC & 100% LEGAL</h2>
        <div class="banner-content">
            <div class="qualified-form">
                <h3>Are you Qualified?</h3>
                  <h4>Find out now.</h4>
                  <small>100% free online assessment!</small>
                <form action="">
                <div class="form-group">
                  <input type="text" class="form-control" id="firstNameInput" placeholder="First Name">
                  <label for="firstNameInput">First Name</label>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" id="EmailInput" placeholder="Email">
                  <label for="firstNameInput">Email</label>
                </div>
                <div class="form-group typeslct-row">
                  <select name="" id="selectPetType" class="form-control">
                      <option value="">--select Options--</option>
                      <option value="">option1</option>
                      <option value="">option2</option>
                  </select>
                  <label for="firstNameInput">Select Pet Type</label>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" id="EmailInput2" placeholder="Email">
                  <label for="firstNameInput">Email</label>
                </div>
                <button type="submit">Get Started Now</button>
                <p>Simple ESA Letter Process</p>
                </form>
                <a href="" class="family_join_btn">Join Our Family Today!</a>
            </div>
            <div class="banner-img">
              <img src="images/banner-img.png" alt="">
            </div>
        </div>
      </div>
  </div>
</section>

<section class="letter-apply">
    <div class="container">
      <div class="row no-gutters justify-content-between">
        <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
          <div class="letter-apply-col">
            <div class="apply-col-img">
              <img src="images/apply-img1.png" alt="">
            </div>
            <div class="apply-col-content text-center">
              <h3>Don't Leave your pet behind!</h3>
              <p>Travel on any U.S. Airline with YOUR PET in the passenger compartment WITH YOU. <b>Federal Statute Air Carrier Access Act (49 U.S.C. 41705 and 14 C.F.R 382)</b> is clear: Airlines must provide “reasonable accommodations” for your well-behaved pet (regardless of size or breed). Our 100% legitimate ESA Letter guarantees your rights under the law!</p>
              <a href="Javascript:void(0)" class="call-to-action">Apply Now</a>
            </div>
          </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
          <div class="letter-apply-col ml-auto mb-0">
            <div class="apply-col-img">
              <img src="images/apply-img2.png" alt="">
            </div>
            <div class="apply-col-content text-center">
              <h3>No more landlord hassles...period.</h3>
              <p><b>Section 504 of the Rehabilitation Act of 1973</b> and <b>Fair Housing Amendments Act of 1988</b> means you and your pet can live where you want to. But you must possess a legitimate, medically-prescribed ESA Letter. That’s exactly the kind of legally-enforceable documents our 50+ licensed therapists provide.</p>
              <a href="Javascript:void(0)" class="call-to-action">Apply Now</a>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>

<section class="change-life">
  <div class="container">
    <div class="change-life-inner text-center">
      <h3>Change your life...and the life of your pet!</h3>
      <p>Order your bona fide ESA Letter now and live anywhere, fly anywhere...with your pet.</p>
      <a href="Javascript:void(0)" class="call-to-action">Apply Now</a>
    </div>
  </div>
</section>

<section class="benefits">
  <div class="back-paws backpaw1"></div>
  <div class="back-paws backpaw2"></div>
  <div class="back-paws backpaw3"></div>
  <div class="back-paws backpaw4"></div>
  <div class="container">
    <div class="row benefits-inner">
      <div class="col-md-12 col-lg-12">
        <h3>The Benefits are Real</h3>
      </div>
      <div class="col-sm-5 col-md-5 col-lg-5 col-xl-5 d-none d-md-block">
        <div class="benefits-col-left">
          <img src="images/benefits-img.png" alt="">
        </div>
      </div>
      <div class="col-sm-12 col-md-7 col-lg-7 col-xl-7">
        <div class="benefits-col-right">
          <ul>
            <li>
              <span><img src="images/benefits-itm1.svg" alt=""></span>
              <h5>No Additional Costs</h5>
              <p>You are guaranteed reasonable rental housing access for you and your pet, regardless of a landlord’s “no pet” policies or the species or size of your well-behaved pet without additional pet fees or deposits.</p>
            </li>
            <li>
              <span><img src="images/benefits-itm2.svg" alt=""></span>
              <h5>Travel In-Cabin</h5>
              <p>You will have access to all domestic airline commercial flights in the company of your well-behaved pet without the imposition of additional airline fees, taxes, or surcharges.</p>
            </li>
            <li>
              <span><img src="images/benefits-itm3.svg" alt=""></span>
              <h5>24/7 Support</h5>
              <p>You are guaranteed the 24/7 love, affection, and support of your pet wherever you go.</p>
            </li>
            <li>
              <span><img src="images/benefits-itm4.svg" alt=""></span>
              <h5>No-Training Required</h5>
              <p>Your pet requires no special training of any kind.</p>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="requirements">
  <div class="back-paws backpaw5"></div>
  <div class="back-paws backpaw6"></div>
  <div class="back-paws backpaw7"></div>
  <div class="container">
      <div class="row">
          <div class="col-sm-12 col-md-10 col-lg-10 col-xl-10 requirements-inner">
              <h3 class="text-sm-center text-md-left">Requirements</h3>
              <p>Your pet must be well-behaved and responsive to your commands.</p>
              <p>You must be in possession of a valid Emotional Support Animal letter that includes:</p>
              <ul>
                <li>
                  <p>Dated within the past twelve months.</p>
                </li>
                <li>
                  <p>A mental health professional’s letterhead, signature, license number.</p>
                </li>
                <li>
                  <p>Professional evaluation indicating your need for your pet’s emotional support.</p>
                </li>
                <li>
                  <p>Recommendation for an emotional support animal.</p>
                </li>
              </ul>
              
          </div>
      </div>
  </div>
</section>

<section class="legal-process">
  <div class="container">
    <div class="legal-prc-inner">
      <h3>Our Legally Certified Process</h3>
      <ul>
        <li class="process-item">
          <div class="process-wrap">
            <div class="item-img">
              <img src="images/prc-item1.svg" alt="">
            </div>
            <p>Complete a simple Questionnaire</p>
          </div>
        </li>
        <li class="process-item">
          <div class="process-wrap">
            <div class="item-img">
              <img src="images/prc-item2.svg" alt="">
            </div>
            <p>If Qualified, Place Your Order</p>
          </div>
        </li>
        <li class="process-item">
          <div class="process-wrap">
            <div class="item-img">
              <img src="images/prc-item3.svg" alt="">
            </div>
            <p>A licensed therapist reviews your application</p>
          </div>
        </li>
        <li class="process-item">
          <div class="process-wrap">
            <div class="item-img">
              <img src="images/prc-item4.svg" alt="">
            </div>
            <p>Receive your ESA letter!</p>
          </div>
        </li>
      </ul>
    </div>
  </div>
</section>

<section class="pricing d-none d-md-block">
  <div class="back-paws backpaw9"></div> 
  <div class="container">
    <div class="row no-gutters justify-content-between">
      <div class="col-sm-12 col-md-12">
        <h3>Choose Your ESA Letter Online</h3>
      </div>
      <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4 pricezoom">
        <div class="pricing-outer">
          <h4>Housing Letter</h4>
          <div class="pricing-content">
            <p>Medically Licensed Therapist Endorsed Housing Letter</p>
            <p>Future Renewal Discounts</p>
            <span class="total-price">
              <span class="dist-price">$199.0</span>
              <span class="new-price">$149.0</span>
            </span>
            <a href="" class="call-to-action">Apply Now</a>
            <p>Prepay 50% option at checkout page</p>
            <p>Same day letter delivery option avaliable</p>
          </div>
        </div>
      </div>
      <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4 pricezoom">
      <div class="pricing-outer mx-auto">
          <h4>Traveling Letter</h4>
          <div class="pricing-content">
            <p>Medically Licensed Therapist Endorsed Housing Letter</p>
            <p>Future Renewal Discounts</p>
            <span class="total-price">
              <span class="dist-price">$199.0</span>
              <span class="new-price">$149.0</span>
            </span>
            <a href="" class="call-to-action">Apply Now</a>
            <p>Prepay 50% option at checkout page</p>
            <p>Same day letter delivery option avaliable</p>
          </div>
        </div>
      </div>
      <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 pricezoom">
        <span class="pop-banner">
            Most popular
        </span>
      <div class="pricing-outer ml-auto">
          <h4>Housing and Travel</h4>
          <div class="pricing-content">
            <p>Covers all housing complexes</p>
            <p>Covers all domestic and international flights originating in the US or run by US airlines</p>
            <p>Lasts for one full year</p>
            <p>No security deposit</p>
            <p>No monthly pet fees</p>
            <p>Future Renewal Discounts</p>
            <span class="total-price">
              <span class="dist-price">$249.0</span>
              <span class="new-price">$199.0</span>
            </span>
            <a href="" class="call-to-action">Apply Now</a>
            <p>Prepay 50% option at checkout page</p>
            <p>Same day letter delivery option avaliable</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="faq_section d-block d-md-none">
  <div class="back-paws backpaw11"></div>
  <div class="back-paws backpaw12"></div>
  <div class="back-paws backpaw13"></div>
  <div class="back-paws backpaw14"></div>
   <div class="container">
      <h3 class="text-center">How to Get a Valid and Legal ESA Letter Online?</h3>
   </div>
   <div class="tabs-outer">
      <div class="container">
         <div class="tabs-area-inner">
               <div class="tabs-area-left">
                  <div class="tabs-left-inner tab-content">
                     <div class="tabs-left-content tab-pane active" id="tab_a">
                        <h2>What Is A College Essay?</h2>  
                        <div class="tab-content-main">
                          <p>A college essay is a part of a college admission application. Along with standardized tests and transcripts, application college essays are also a part of many college applications. It is considered as one of the most stressful tasks. A good college essay shows who you really are and why you deserve the respective college admission.</p>
                          <p>What should you add into your college essay? It should contain the following elements:</p>
                          <ul>
                            <li>In-depth insight into your personality i.e. your personal history and future goals.</li>
                            <li>Extraordinary and descriptive writing skills that can help you add details into your essay</li>
                            <li>Detail about your life story that is relevant to your reason of applying in the college</li>
                            <li>Your main reasons for applying to a particular college</li>
                            <li>Ways the education will affect your future life and goals</li>
                            <li>The way the college will help build your personality and help you add value to the society</li>
                          </ul>
                        </div>
                     </div>
                     <div class="tabs-left-content tab-pane" id="tab_b">
                        <h2>What Should I Write About In My College Essay?</h2>
                        <div class="tab-content-main">
                          <p>A college essay is a part of a college admission application. Along with standardized tests and transcripts, application college essays are also a part of many college applications. It is considered as one of the most stressful tasks. A good college essay shows who you really are and why you deserve the respective college admission.</p>
                          <p>What should you add into your college essay? It should contain the following elements:</p>
                          <ul>
                            <li>In-depth insight into your personality i.e. your personal history and future goals.</li>
                            <li>Extraordinary and descriptive writing skills that can help you add details into your essay</li>
                            <li>Detail about your life story that is relevant to your reason of applying in the college</li>
                            <li>Your main reasons for applying to a particular college</li>
                            <li>Ways the education will affect your future life and goals</li>
                            <li>The way the college will help build your personality and help you add value to the society</li>
                          </ul>
                        </div>
                     </div>
                     <div class="tabs-left-content tab-pane" id="tab_c">
                        <h2>What You Should Not Write In A College Essay?</h2>
                        <div class="tab-content-main">
                          <p>A college essay is a part of a college admission application. Along with standardized tests and transcripts, application college essays are also a part of many college applications. It is considered as one of the most stressful tasks. A good college essay shows who you really are and why you deserve the respective college admission.</p>
                          <p>What should you add into your college essay? It should contain the following elements:</p>
                          <ul>
                            <li>In-depth insight into your personality i.e. your personal history and future goals.</li>
                            <li>Extraordinary and descriptive writing skills that can help you add details into your essay</li>
                            <li>Detail about your life story that is relevant to your reason of applying in the college</li>
                            <li>Your main reasons for applying to a particular college</li>
                            <li>Ways the education will affect your future life and goals</li>
                            <li>The way the college will help build your personality and help you add value to the society</li>
                          </ul>
                        </div>
                     </div>
                     <div class="tabs-left-content tab-pane" id="tab_d">
                        <h2>How To Write A College Essay?</h2>
                        <div class="tab-content-main">
                          <p>A college essay is a part of a college admission application. Along with standardized tests and transcripts, application college essays are also a part of many college applications. It is considered as one of the most stressful tasks. A good college essay shows who you really are and why you deserve the respective college admission.</p>
                          <p>What should you add into your college essay? It should contain the following elements:</p>
                          <ul>
                            <li>In-depth insight into your personality i.e. your personal history and future goals.</li>
                            <li>Extraordinary and descriptive writing skills that can help you add details into your essay</li>
                            <li>Detail about your life story that is relevant to your reason of applying in the college</li>
                            <li>Your main reasons for applying to a particular college</li>
                            <li>Ways the education will affect your future life and goals</li>
                            <li>The way the college will help build your personality and help you add value to the society</li>
                          </ul>
                        </div>
                     </div>
                     <div class="tabs-left-content tab-pane" id="tab_e">
                        <h2>College Essay Tips</h2>
                        <div class="tab-content-main">
                          <p>A college essay is a part of a college admission application. Along with standardized tests and transcripts, application college essays are also a part of many college applications. It is considered as one of the most stressful tasks. A good college essay shows who you really are and why you deserve the respective college admission.</p>
                          <p>What should you add into your college essay? It should contain the following elements:</p>
                          <ul>
                            <li>In-depth insight into your personality i.e. your personal history and future goals.</li>
                            <li>Extraordinary and descriptive writing skills that can help you add details into your essay</li>
                            <li>Detail about your life story that is relevant to your reason of applying in the college</li>
                            <li>Your main reasons for applying to a particular college</li>
                            <li>Ways the education will affect your future life and goals</li>
                            <li>The way the college will help build your personality and help you add value to the society</li>
                          </ul>
                        </div>
                     </div>
                     <div class="tabs-left-content tab-pane" id="tab_f">
                        <h2>Get Your College Essay Written by Experts</h2>
                        <div class="tab-content-main">
                          <p>A college essay is a part of a college admission application. Along with standardized tests and transcripts, application college essays are also a part of many college applications. It is considered as one of the most stressful tasks. A good college essay shows who you really are and why you deserve the respective college admission.</p>
                          <p>What should you add into your college essay? It should contain the following elements:</p>
                          <ul>
                            <li>In-depth insight into your personality i.e. your personal history and future goals.</li>
                            <li>Extraordinary and descriptive writing skills that can help you add details into your essay</li>
                            <li>Detail about your life story that is relevant to your reason of applying in the college</li>
                            <li>Your main reasons for applying to a particular college</li>
                            <li>Ways the education will affect your future life and goals</li>
                            <li>The way the college will help build your personality and help you add value to the society</li>
                          </ul>
                        </div>
                     </div>
                     <div class="tabs-left-content tab-pane" id="tab_g">
                        <h2>College Essay Tips to Write a Strong Essay </h2>
                        <div class="tab-content-main">
                          <p>A college essay is a part of a college admission application. Along with standardized tests and transcripts, application college essays are also a part of many college applications. It is considered as one of the most stressful tasks. A good college essay shows who you really are and why you deserve the respective college admission.</p>
                          <p>What should you add into your college essay? It should contain the following elements:</p>
                          <ul>
                            <li>In-depth insight into your personality i.e. your personal history and future goals.</li>
                            <li>Extraordinary and descriptive writing skills that can help you add details into your essay</li>
                            <li>Detail about your life story that is relevant to your reason of applying in the college</li>
                            <li>Your main reasons for applying to a particular college</li>
                            <li>Ways the education will affect your future life and goals</li>
                            <li>The way the college will help build your personality and help you add value to the society</li>
                          </ul>
                        </div>
                     </div>
                     <div class="tabs-left-content tab-pane" id="tab_h">
                        <h2>Get Professional Help for Your College Essay</h2>
                        <div class="tab-content-main">
                          <p>A college essay is a part of a college admission application. Along with standardized tests and transcripts, application college essays are also a part of many college applications. It is considered as one of the most stressful tasks. A good college essay shows who you really are and why you deserve the respective college admission.</p>
                          <p>What should you add into your college essay? It should contain the following elements:</p>
                          <ul>
                            <li>In-depth insight into your personality i.e. your personal history and future goals.</li>
                            <li>Extraordinary and descriptive writing skills that can help you add details into your essay</li>
                            <li>Detail about your life story that is relevant to your reason of applying in the college</li>
                            <li>Your main reasons for applying to a particular college</li>
                            <li>Ways the education will affect your future life and goals</li>
                            <li>The way the college will help build your personality and help you add value to the society</li>
                          </ul>
                        </div>
                     </div>
                  </div>
                  <div class="tab-img">
                       <img src="images/tab-img.png" alt="dog-img">
                  </div>
               </div>
               <div class="tabs-area-right">
                  <ul class="nav nav-pills nav-stacked tabs-right-nav">
                     <li class=""><a href="#tab_a" class="ques_tabs" data-toggle="pill" role="tab" aria-controls="tab_a">What Is A College Essay?</a></li>
                     <li class=""><a href="#tab_b" class="ques_tabs" data-toggle="pill" role="tab" aria-controls="tab_b">What Should I Write About In My College Essay?</a></li>
                     <li class=""><a href="#tab_c" class="ques_tabs" data-toggle="pill" role="tab" aria-controls="tab_c">What You Should Not Write In A College Essay?</a></li>
                     <li class=""><a href="#tab_d" class="ques_tabs" data-toggle="pill" role="tab" aria-controls="tab_d">How To Write A College Essay?</a></li>
                     <li class=""><a href="#tab_e" class="ques_tabs" data-toggle="pill" role="tab" aria-controls="tab_e">College Essay Tips</a></li>
                     <li class=""><a href="#tab_f" class="ques_tabs" data-toggle="pill" role="tab" aria-controls="tab_f">Get Your College Essay Written by Experts</a></li>
                     <li class=""><a href="#tab_g" class="ques_tabs" data-toggle="pill" role="tab" aria-controls="tab_g">College Essay Tips to Write a Strong Essay</a></li>
                     <li class=""><a href="#tab_h" class="ques_tabs" data-toggle="pill" role="tab" aria-controls="tab_h">Get Professional Help for Your College Essay</a></li>
                  </ul>
               </div>
         </div>
      </div>
   </div>
</div>

<section class="testmonial">
  <div class="back-paws backpaw10 d-none d-md-block"></div>
  <div class="container">
      <h3 class="text-center">Join the MyESAletter.com Family Today</h3>
    <div class="testmonial-outer">
      <div class="testmonial-inner">
        <div class="swiper-container testmonial-main">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <div class="test-slide-inner">
                <div class="testmonial-img">
                  <img src="images/testmonial-img.png" alt="">
                </div>
                <div class="testmonial-content">
                  <h5>Elliot Z</h5>
                  <small>Denver, Co.</small>
                  <p>We were leaving on vacation in less than a week, and our kennel wouldn't take our Lab (Coco) because her shots weren't up to date. We applied for the ESA Letter on a "rush" basis, and we actually took Coco with us to California! Amazing service.</p>
                  <div class="star-group">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="test-slide-inner">
                  <div class="testmonial-img">
                    <img src="images/testmonial-img.png" alt="">
                  </div>
                  <div class="testmonial-content">
                    <h5>Elliot W</h5>
                    <small>Denver, Co.</small>
                    <p>We were leaving on vacation in less than a week, and our kennel wouldn't take our Lab (Coco) because her shots weren't up to date. We applied for the ESA Letter on a "rush" basis, and we actually took Coco with us to California! Amazing service.</p>
                    <div class="star-group">
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                    </div>
                  </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="test-slide-inner">
                  <div class="testmonial-img">
                    <img src="images/testmonial-img.png" alt="">
                  </div>
                  <div class="testmonial-content">
                    <h5>Elliot X</h5>
                    <small>Denver, Co.</small>
                    <p>We were leaving on vacation in less than a week, and our kennel wouldn't take our Lab (Coco) because her shots weren't up to date. We applied for the ESA Letter on a "rush" basis, and we actually took Coco with us to California! Amazing service.</p>
                    <div class="star-group">
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
            </div>
            <div class="swiper-slide">
              <div class="test-slide-inner">
                  <div class="testmonial-img">
                    <img src="images/testmonial-img.png" alt="">
                  </div>
                  <div class="testmonial-content">
                    <h5>Elliot Y</h5>
                    <small>Denver, Co.</small>
                    <p>We were leaving on vacation in less than a week, and our kennel wouldn't take our Lab (Coco) because her shots weren't up to date. We applied for the ESA Letter on a "rush" basis, and we actually took Coco with us to California! Amazing service.</p>
                    <div class="star-group">
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
            </div>
          </div>
          <div class="swiper-button-prev"><i class="fa fa-long-arrow-up" aria-hidden="true"></i></div>
          <span class="test-btn-txt">Next</span>
          <div class="swiper-button-next"><i class="fa fa-long-arrow-down" aria-hidden="true"></i></div>
        </div>
        <div class="swiper-container testmonial-thumbs">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <div class="test-thumb-img">
                <img src="https://picsum.photos/seed/slide1/115/100" alt="Slide 01">
              </div>
            </div>
            <div class="swiper-slide">
              <div class="test-thumb-img">
                <img src="https://picsum.photos/seed/slide2/115/100" alt="Slide 02">
              </div>
            </div>
            <div class="swiper-slide">
              <div class="test-thumb-img">
                <img src="https://picsum.photos/seed/slide3/115/100" alt="Slide 03">
              </div>
            </div>
            <div class="swiper-slide">
              <div class="test-thumb-img">
                <img src="https://picsum.photos/seed/slide4/115/100" alt="Slide 04">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="faq_section d-none d-md-block">
  <div class="back-paws backpaw11"></div>
  <div class="back-paws backpaw12"></div>
  <div class="back-paws backpaw13"></div>
  <div class="back-paws backpaw14"></div>
   <div class="container">
      <h3 class="text-center">How to Get a Valid and Legal ESA Letter Online?</h3>
   </div>
   <div class="tabs-outer">
      <div class="container">
         <div class="tabs-area-inner">
               <div class="tabs-area-left">
                  <div class="tabs-left-inner tab-content">
                     <div class="tabs-left-content tab-pane active" id="tab_a">
                        <h2>What Is A College Essay?</h2>  
                        <div class="tab-content-main">
                          <p>A college essay is a part of a college admission application. Along with standardized tests and transcripts, application college essays are also a part of many college applications. It is considered as one of the most stressful tasks. A good college essay shows who you really are and why you deserve the respective college admission.</p>
                          <p>What should you add into your college essay? It should contain the following elements:</p>
                          <ul>
                            <li>In-depth insight into your personality i.e. your personal history and future goals.</li>
                            <li>Extraordinary and descriptive writing skills that can help you add details into your essay</li>
                            <li>Detail about your life story that is relevant to your reason of applying in the college</li>
                            <li>Your main reasons for applying to a particular college</li>
                            <li>Ways the education will affect your future life and goals</li>
                            <li>The way the college will help build your personality and help you add value to the society</li>
                          </ul>
                        </div>
                     </div>
                     <div class="tabs-left-content tab-pane" id="tab_b">
                        <h2>What Should I Write About In My College Essay?</h2>
                        <div class="tab-content-main">
                          <p>A college essay is a part of a college admission application. Along with standardized tests and transcripts, application college essays are also a part of many college applications. It is considered as one of the most stressful tasks. A good college essay shows who you really are and why you deserve the respective college admission.</p>
                          <p>What should you add into your college essay? It should contain the following elements:</p>
                          <ul>
                            <li>In-depth insight into your personality i.e. your personal history and future goals.</li>
                            <li>Extraordinary and descriptive writing skills that can help you add details into your essay</li>
                            <li>Detail about your life story that is relevant to your reason of applying in the college</li>
                            <li>Your main reasons for applying to a particular college</li>
                            <li>Ways the education will affect your future life and goals</li>
                            <li>The way the college will help build your personality and help you add value to the society</li>
                          </ul>
                        </div>
                     </div>
                     <div class="tabs-left-content tab-pane" id="tab_c">
                        <h2>What You Should Not Write In A College Essay?</h2>
                        <div class="tab-content-main">
                          <p>A college essay is a part of a college admission application. Along with standardized tests and transcripts, application college essays are also a part of many college applications. It is considered as one of the most stressful tasks. A good college essay shows who you really are and why you deserve the respective college admission.</p>
                          <p>What should you add into your college essay? It should contain the following elements:</p>
                          <ul>
                            <li>In-depth insight into your personality i.e. your personal history and future goals.</li>
                            <li>Extraordinary and descriptive writing skills that can help you add details into your essay</li>
                            <li>Detail about your life story that is relevant to your reason of applying in the college</li>
                            <li>Your main reasons for applying to a particular college</li>
                            <li>Ways the education will affect your future life and goals</li>
                            <li>The way the college will help build your personality and help you add value to the society</li>
                          </ul>
                        </div>
                     </div>
                     <div class="tabs-left-content tab-pane" id="tab_d">
                        <h2>How To Write A College Essay?</h2>
                        <div class="tab-content-main">
                          <p>A college essay is a part of a college admission application. Along with standardized tests and transcripts, application college essays are also a part of many college applications. It is considered as one of the most stressful tasks. A good college essay shows who you really are and why you deserve the respective college admission.</p>
                          <p>What should you add into your college essay? It should contain the following elements:</p>
                          <ul>
                            <li>In-depth insight into your personality i.e. your personal history and future goals.</li>
                            <li>Extraordinary and descriptive writing skills that can help you add details into your essay</li>
                            <li>Detail about your life story that is relevant to your reason of applying in the college</li>
                            <li>Your main reasons for applying to a particular college</li>
                            <li>Ways the education will affect your future life and goals</li>
                            <li>The way the college will help build your personality and help you add value to the society</li>
                          </ul>
                        </div>
                     </div>
                     <div class="tabs-left-content tab-pane" id="tab_e">
                        <h2>College Essay Tips</h2>
                        <div class="tab-content-main">
                          <p>A college essay is a part of a college admission application. Along with standardized tests and transcripts, application college essays are also a part of many college applications. It is considered as one of the most stressful tasks. A good college essay shows who you really are and why you deserve the respective college admission.</p>
                          <p>What should you add into your college essay? It should contain the following elements:</p>
                          <ul>
                            <li>In-depth insight into your personality i.e. your personal history and future goals.</li>
                            <li>Extraordinary and descriptive writing skills that can help you add details into your essay</li>
                            <li>Detail about your life story that is relevant to your reason of applying in the college</li>
                            <li>Your main reasons for applying to a particular college</li>
                            <li>Ways the education will affect your future life and goals</li>
                            <li>The way the college will help build your personality and help you add value to the society</li>
                          </ul>
                        </div>
                     </div>
                     <div class="tabs-left-content tab-pane" id="tab_f">
                        <h2>Get Your College Essay Written by Experts</h2>
                        <div class="tab-content-main">
                          <p>A college essay is a part of a college admission application. Along with standardized tests and transcripts, application college essays are also a part of many college applications. It is considered as one of the most stressful tasks. A good college essay shows who you really are and why you deserve the respective college admission.</p>
                          <p>What should you add into your college essay? It should contain the following elements:</p>
                          <ul>
                            <li>In-depth insight into your personality i.e. your personal history and future goals.</li>
                            <li>Extraordinary and descriptive writing skills that can help you add details into your essay</li>
                            <li>Detail about your life story that is relevant to your reason of applying in the college</li>
                            <li>Your main reasons for applying to a particular college</li>
                            <li>Ways the education will affect your future life and goals</li>
                            <li>The way the college will help build your personality and help you add value to the society</li>
                          </ul>
                        </div>
                     </div>
                     <div class="tabs-left-content tab-pane" id="tab_g">
                        <h2>College Essay Tips to Write a Strong Essay </h2>
                        <div class="tab-content-main">
                          <p>A college essay is a part of a college admission application. Along with standardized tests and transcripts, application college essays are also a part of many college applications. It is considered as one of the most stressful tasks. A good college essay shows who you really are and why you deserve the respective college admission.</p>
                          <p>What should you add into your college essay? It should contain the following elements:</p>
                          <ul>
                            <li>In-depth insight into your personality i.e. your personal history and future goals.</li>
                            <li>Extraordinary and descriptive writing skills that can help you add details into your essay</li>
                            <li>Detail about your life story that is relevant to your reason of applying in the college</li>
                            <li>Your main reasons for applying to a particular college</li>
                            <li>Ways the education will affect your future life and goals</li>
                            <li>The way the college will help build your personality and help you add value to the society</li>
                          </ul>
                        </div>
                     </div>
                     <div class="tabs-left-content tab-pane" id="tab_h">
                        <h2>Get Professional Help for Your College Essay</h2>
                        <div class="tab-content-main">
                          <p>A college essay is a part of a college admission application. Along with standardized tests and transcripts, application college essays are also a part of many college applications. It is considered as one of the most stressful tasks. A good college essay shows who you really are and why you deserve the respective college admission.</p>
                          <p>What should you add into your college essay? It should contain the following elements:</p>
                          <ul>
                            <li>In-depth insight into your personality i.e. your personal history and future goals.</li>
                            <li>Extraordinary and descriptive writing skills that can help you add details into your essay</li>
                            <li>Detail about your life story that is relevant to your reason of applying in the college</li>
                            <li>Your main reasons for applying to a particular college</li>
                            <li>Ways the education will affect your future life and goals</li>
                            <li>The way the college will help build your personality and help you add value to the society</li>
                          </ul>
                        </div>
                     </div>
                  </div>
                  <div class="tab-img">
                       <img src="images/tab-img.png" alt="dog-img">
                  </div>
               </div>
               <div class="tabs-area-right">
                  <ul class="nav nav-pills nav-stacked tabs-right-nav">
                     <li class=""><a href="#tab_a" class="ques_tabs" data-toggle="pill" role="tab" aria-controls="tab_a">What Is A College Essay?</a></li>
                     <li class=""><a href="#tab_b" class="ques_tabs" data-toggle="pill" role="tab" aria-controls="tab_b">What Should I Write About In My College Essay?</a></li>
                     <li class=""><a href="#tab_c" class="ques_tabs" data-toggle="pill" role="tab" aria-controls="tab_c">What You Should Not Write In A College Essay?</a></li>
                     <li class=""><a href="#tab_d" class="ques_tabs" data-toggle="pill" role="tab" aria-controls="tab_d">How To Write A College Essay?</a></li>
                     <li class=""><a href="#tab_e" class="ques_tabs" data-toggle="pill" role="tab" aria-controls="tab_e">College Essay Tips</a></li>
                     <li class=""><a href="#tab_f" class="ques_tabs" data-toggle="pill" role="tab" aria-controls="tab_f">Get Your College Essay Written by Experts</a></li>
                     <li class=""><a href="#tab_g" class="ques_tabs" data-toggle="pill" role="tab" aria-controls="tab_g">College Essay Tips to Write a Strong Essay</a></li>
                     <li class=""><a href="#tab_h" class="ques_tabs" data-toggle="pill" role="tab" aria-controls="tab_h">Get Professional Help for Your College Essay</a></li>
                  </ul>
               </div>
         </div>
      </div>
   </div>
</div>

<section class="sample-section d-none d-md-block">
  <div class="back-paws backpaw15"></div>
  <div class="back-paws backpaw16"></div>
  <div class="container">
  <h3 class="text-center">Sample ESA Letter</h3>
    <div class="sample-section-inner pt-5">
      <ul>
        <li>1</li>
        <li>2</li>
        <li>3</li>
      </ul>
      <div class="row pt-5">
        <div class="col-md-4 col-lg-4">
          <div class="sample-items mx-auto">
            <h5>Sample ESA Letter</h5>
            <img src="images/sample-item-img1.svg" alt="">
            <div class="sample-item-cta">
              <a href="">View in PDF</a>
              <a href="">Apply Now</a>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-lg-4">
            <div class="sample-items mx-auto">
              <h5>ESA Housing Letter</h5>
              <img src="images/sample-item-img2.svg" alt="">
              <div class="sample-item-cta">
                <a href="">View in PDF</a>
                <a href="">Apply Now</a>
              </div>
            </div>
        </div>
        <div class="col-md-4 col-lg-4">
          <div class="sample-items mx-auto">
            <h5>Airline Sample Letter</h5>
            <img src="images/sample-item-img3.svg" alt="">
            <div class="sample-item-cta">
              <a href="">View in PDF</a>
              <a href="">Apply Now</a>
            </div>
          </div>
        </div>
      </div>
      <div class="row apply-complaint">
        <div class="col-md-12 col-lg-12 col-xl-12 text-center">
          <p>You can have your legally compliant ESA letter in hand within 24 hours, so</p>
          <a href="" class="call-to-action">Apply Now</a>
          <p class="pt-4">Don’t leave the one who truly loves you behind!</p>
        </div>
      </div>
    </div>
  </div>
</section>


<?php include("foot.php"); ?>
<?php include("footer.php"); ?>
